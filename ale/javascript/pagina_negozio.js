$.getJSON("../php/pagina_negozio.php?request=session", function(data) {
  if(data[0] == 0){
    window.location.replace("../../ale/html/login.html");
  }
});

function searchbar(){
  if($("#search").val().length > 0){
    $("div#Menu li").each(function(index) {
      if(!$(this).find("label").attr("class").toLowerCase().startsWith($("#search").val().toLowerCase())){
        $(this).css("display","none");
      } else {
        $(this).css("display","block");
      }
    });
  }else{
    $("div#Menu li").each(function(index) {
      $(this).css("display","block");
    });
  }
}

function openBar(evt, elem) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(elem).style.display = "block";
  evt.currentTarget.className += " active";
};

$(document).ready(function(){
  ready();
  $('header').on("keyup", "#search", searchbar);
});

function ready(){
  openBar(window.event,'Menu');
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=forn&negozio="+param, function(data) {
    var html_code = "<ul>";
    for(var i = 0; i < data.length; i++){
      html_code += '<li>';
      html_code += '<label class="'+data[i]["nome"]+'">'+data[i]["nome"]+'</label>';
      html_code += '<label for="value'+data[i]["id"]+'" hidden>Prezzo</label>';
      html_code += '<input type="text" id="value'+data[i]["id"]+'" name="prezzo" value="'+data[i]["prezzo"]+' €" disabled>';
      html_code += '<label>'+data[i]["descrizione"]+'</label>';
      html_code += '<label for="'+data[i]["id"]+'" hidden>Quantità</label>';
      html_code += '<input type="number" min="0" step="1" name="'+data[i]["id"]+'" id="'+data[i]["id"]+'" placeholder="Quantità"> </input>';
      html_code += '<button type="button" class="action-button shadow animate orange" onclick="send('+data[i]["id"]+')"> Aggiungi al carrello </button>';
      html_code += '</li>';
    }
    html_code += '</ul>'
    // var html_code = "";
    // for(var i = 0; i < data.length; i++){
    //   html_code +='<label>'+data[i]["nome"]+'</label>'
    //   html_code += '<li><label >'+data[i]["descrizione"]+'</label>';
    //   html_code += '<input type="number" disabled id="value'+data[i]["id"]+'" name="prezzo" min="1.00" max="100.00" step="0.10" value='+data[i]["prezzo"]+'>€ <button type="button" onclick="send('+data[i]["id"]+
    // 			')"> Aggiungi al carrello </button>  <input type="text" name="'+data[i]["id"]+'" id="'+data[i]["id"]+'" placeholder="Inserisci quantità"> </input> </li> </ul>';
    // }
    $("#Menu").html(html_code);
  });
  //VIASUALIZZARE E AGGIUNGERE RECENSIONI
  // openBar(window.event,'Recensioni');
  var url = new URL(document.URL);
  var fornitore = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=get&param="+fornitore, function(data) {
    var html_code = "";
    html_code += '<h2>Inserisci la tua recensione!</h2>';
    html_code += '<label>Punteggio </label> <div id="stelle"><span id="1stella" class="fa fa-star checked"></span><span id="2stella" class="fa fa-star"></span><span id="3stella" class="fa fa-star"></span><span id="4stella" class="fa fa-star"></span><span id="5stella" class="fa fa-star"></span> </div>'+
    '<label for="recens">Recensione </label> <input id="recens" type="text" placeholder="recensione"> </input>'+
    '<button type="button" id="'+fornitore+'" class="action-button shadow animate orange"> Aggiungi una recensione </button>';
    $("#Recensioni").html(html_code);
    $(".fa-star").on("click",function(id){
      $(this).addClass("checked");
      $(this).prevAll().addClass("checked");
      $(this).nextAll().removeClass("checked");
    });
    html_code = "";
    for(var i = 0; i < data.length; i++){
      html_code += '<div class="containerReviews">';
      html_code += '<h3><span>'+data[i]["userUtente"]+'</span> </h3>';
      var stelle = data[i]["Stelle"];
      for(j=1;j<=5;j++){
        if(j<=stelle){
          html_code += '<span class="fa fa-star checked" ></span>';
        }else{
          html_code += '<span class="fa fa-star"></span>';
        }
      }
      html_code += '<p>'+data[i]["recenz"]+'</p>'
      html_code += '</div>';
    }
    $("#Recensioni").append(html_code);

  });

  $("div#Recensioni").on("click","button",send_rec);
  //Info
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=info&param="+param, function(data) {
    $("h1#nome_negozio").text("");
    $("h1#nome_negozio").text(data["nome_negozio"]);

    var html_code = "";
    html_code += '<label for="name">Nome</label>' +
    '<input type="text" class="input" id="name" value="'+data["nome"]+'" disabled/>' +
    '<label for="surname">Cognome</label>' +
    '<input type="text" class="input" id="surname" value="'+data["cognome"]+'" disabled/>' +
    '<label for="email">Email</label>' +
    '<input type="email" class="input" id="email" value="'+data["e-mail"]+'" disabled/>' +
    '<label for="iva">Partita Iva</label>' +
    '<input type="text" class="input" id="iva" value="'+data["partita_iva"]+'" disabled/>';
    $("#Info").html(html_code);
  });

  //Orario
  var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=fascia&param="+param, function(data) {
    var html_code = "";
    html_code += '<table>'+
    '<thead>'+
    '<tr>'+
    '<th>Ora Inizio</th>'+
    '<th>Ora Fine</th>'+
    '<th>Numero Clienti</th>'+
    '</tr>'+
    '</thead>'+
    '<tbody id="contenuto">'+
    '</tbody>'+
    '</table>';
    $("#Orario").html(html_code);

    for(var i = 0; i < data.length; i++) {
      html_code = "";
      html_code += '<tr>'+
      '<td>'+data[i]["ora_inizio"].substring(0, 5)+'</td>'+
      '<td>'+data[i]["ora_fine"].substring(0, 5)+'</td>'+
      '<td>'+data[i]["n_clienti"]+'</td>'+
      '</tr>';
      $("tbody#contenuto").append(html_code);
    }
  });
}

function send(id){
  var myid = id;
  var qt = $("input#"+id+"").val();
  $.getJSON("../php/pagina_negozio.php?request=send&id="+id+"&qt="+qt, function(data){
  });
}

function showReviews(){
  var url = new URL(document.URL);
  var fornitore = url.searchParams.get("user");
  $.getJSON("../php/pagina_negozio.php?request=get&param="+fornitore, function(data) {
    var html_code = "";
    html_code += '<h2>Inserisci la tua recensione!</h2>';
    html_code += '<label>Punteggio </label> <div id="stelle"><span id="1stella" class="fa fa-star checked"></span><span id="2stella" class="fa fa-star"></span><span id="3stella" class="fa fa-star"></span><span id="4stella" class="fa fa-star"></span><span id="5stella" class="fa fa-star"></span> </div>'+
    '<label for="recens">Recensione </label> <input id="recens" type="text" placeholder="recensione"> </input>'+
    '<button type="button" id="'+fornitore+'" class="action-button shadow animate orange"> Aggiungi una recensione </button>';
    $("#Recensioni").html(html_code);
    $(".fa-star").on("click",function(id){
      $(this).addClass("checked");
      $(this).prevAll().addClass("checked");
      $(this).nextAll().removeClass("checked");
    });
    html_code = "";
    for(var i = 0; i < data.length; i++){
      html_code += '<div class="containerReviews">';
      html_code += '<h3><span>'+data[i]["userUtente"]+'</span> </h3>';
      var stelle = data[i]["Stelle"];
      for(j=1;j<=5;j++){
        if(j<=stelle){
          html_code += '<span class="fa fa-star checked" ></span>';
        }else{
          html_code += '<span class="fa fa-star"></span>';
        }
      }
      html_code += '<p>'+data[i]["recenz"]+'</p>'
      html_code += '</div>';
    }
    $("#Recensioni").append(html_code);

  });
}
function send_rec(){
  /*var url = new URL(document.URL);
  var param = url.searchParams.get("user");
  var punt;
  var rec;*/
  var fornitore = $("div#Recensioni button").attr("id");
  var rec = $("input#recens").val();
  punt = $("#stelle .checked").length;
  console.log(rec);
  $.getJSON("../php/pagina_negozio.php?request=send_rec&id="+fornitore+"&stelle="+punt+"&recens="+rec, function(data){
    showReviews();
  });
}
