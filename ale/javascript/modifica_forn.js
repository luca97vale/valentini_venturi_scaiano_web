function validateEmail(email)
{
  var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  return regex.test(email);
}

$.getJSON("/luca/php/pietanzeFornitore.php?request=session", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html");
  }
});

$(document).ready(function(){
  html_code = "";
  document.getElementById("mex").style.display="none";
  $.getJSON("../php/modifica_forn.php?request=current", function(data) {
    var user = data[0]['user'];
    var nome = data[0]['nome'];
    var cognome = data[0]['cognome'];
    var partitaiva = data[0]['partita_iva'];
    var email = data[0]['e-mail'];
    var pwd = data[0]['pwd'];
    var nome_negozio = data[0]["nome_negozio"];
    var img = "/filo/base.jpg";
    if (data[0]["image"] != null) {
      img = data[0]["image"];
    }

    $("form#prev").before('<img src="'+img+'" alt="Immagine del profilo">');

    html_code = '<label for="inputUser">User</label>'+ '<input disabled type="text" id="inputUser" value="'+ user + '"> </input>'+
    '<label for="inputNome">Nome</label>'+'<input type="text" id="inputNome" value="'+ nome + '"> </input>' +
    '<label for="inputCognome">Cognome</label>'+'<input type="text" id="inputCognome" value="'+ cognome + '"> </input>' +
    '<label for="inputEmail">E-Mail</label>'+ '<input type="text" id="inputEmail" value="'+ email + '"> </input>' +
    '<label for="inputPwd"> Modifica password </label> <input type="password" id="inputPwd" placeholder="Modifica password"> </input>' +
    '<label for="inputVerpwd"> Conferma password </label> <input type="password" id="inputVerpwd" placeholder="Conferma nuova password"> </input>'+
    '<label for="inputIva">Partita Iva</label>'+ '<input type="text" id="inputIva" value="'+ partitaiva + '"> </input>'+
    '<label for="inputNomeNegozio">Nome negozio</label>'+'<input type="text" id="inputNomeNegozio" value="'+ nome_negozio + '"> </input>' +
    '<label for="inputImg">Immagine profilo </label> <input type="file" id="inputImg"> </input>' +
    '<button type="button" id="fascia" class="action-button shadow animate orange">Fascia oraria</button>';
    $("#update").before(html_code);
  });
  $("#prev").on("click", "#fascia", function() {
    window.location.replace("/luca/html/fascia_oraria.html");
  });
  $("#update").click(function(){
    var nome = $("input#inputNome").val();
    var cognome = $("input#inputCognome").val();
    var newemail = $("input#inputEmail").val();
    var newpwd = $("input#inputPwd").val();
    var verpwd = $("input#inputVerpwd").val();
    var iva = $("input#inputIva").val();
    var negozio = $("input#inputNomeNegozio").val();
    var img = $("#inputImg")[0].files[0];

    $.getJSON("../php/modifica_forn.php?request=update&newpwd="+newpwd+"&newemail="+newemail+"&verpwd="+verpwd+"&nome="+nome+"&cognome="+cognome+"&iva="+iva+"&negozio="+negozio, function(mex) {
      $("div#mex").html(mex);
      document.getElementById("mex").style.display="block";

      if (typeof img != 'undefined') {
        var res = "<p>L'immagine del profilo è stata cambiata</p>";
        reader = new FileReader();
        reader.onload = function(evt) {
          var image = evt.target.result;
          $.post("../php/modifica_forn.php?request=editImage", {img:image}, function(data) {
            $("div#mex").append(res);
            $("#page img").attr("src", image);
          }), "text";
        };
        reader.readAsDataURL(new Blob([img]));
      } else {
        var res = "<p>L'immagine del profilo non è stata cambiata</p>";
        $("div#mex").append(res);
      }
      //document.getElementById("succ").style.display="block";
      //$("form#succ").show();
      //$("form#action").hide();
    });
  });
});
function fascia_oraria(){
  window.location.replace("/luca/html/fascia_oraria.html");
}
function rmNotification(){
  $.getJSON("/luca/php/notification.php?request=rm", function(data) {
  });
}
