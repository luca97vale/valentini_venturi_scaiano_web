function validateEmail(email)
{
  var regex = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
  return regex.test(email);
}

function hasWhiteSpace(s) {
  return s.indexOf(' ') >= 0;
}

$(document).ready(function(){
$("div.alert-js-check").hide();
});

function check(){
    errors = "";

    var nome = $("input#inputNome").val();
    var cognome = $("input#inputCognome").val();
    var email = $("input#inputEmail").val();
    var nomeUtente = $("input#inputUsername").val();
    var pwd = $("input#inputPwd").val();
    var verpwd = $("input#inputVerPassword").val();
	var err_user = "";
	var err_piva = "";
	var err_email = "";
	var err_negozio = "";


	 if(nome == ''){
      errors += "Nome è obbligatorio!<br/>";
    }

    if(cognome == ''){
      errors += "Cognome è obbligatorio!<br/>";
    }

	if(pwd == ''){
		errors += "Password obbligatoria <br/>";
	  }

	if(verpwd == ''){
		errors += "Inserire password per verifica! <br/>";
	}

	if(pwd != verpwd){
		errors += "Le due password inserite sono diverse! <br/>";
	}

$("div.alert-js-check p.user").html("");
    if(nomeUtente == '' || hasWhiteSpace(nomeUtente)){
		var j = $("div.alert-js-check p.user").html(err_user);
		errors += "Username è obbligatorio e non può contenere spazi! <br/>";
    } else{ $.getJSON("../php/reg_utente.php?request=checkuser&user="+nomeUtente, function(query1) {
		if(query1 == 1){
		err_user = "Username già esistente!";
		$("div.alert-js-check p.user").html(err_user);
		$("div.alert-js-check").show();
	}
	});
	}

    $("div.alert-js-check p.email").html("");
	if(email == '' || !validateEmail(email)){
		var n = $("div.alert-js-check p.email").html(err_email);
      errors += "Email è obbligatoria e deve essere valida<br/>";
    }else{ $.getJSON("../php/reg_utente.php?request=checkemail&email="+email, function(query2) {
		if(query2 == 1){
		err_email = "Email già esistente!";
		$("div.alert-js-check p.email").html(err_email);
		$("div.alert-js-check").show();
	}
	});
	}

  if(errors.length > 0)
  {
    var nome = $("div.alert-js p").html(errors);
    $("div.alert-js").show();
  }else{
			  $.getJSON("../php/reg_utente.php?request=checkemail&email="+email, function(query2) {
				  if (query2 ==0){
							  $.getJSON("../php/reg_utente.php?request=checkuser&user="+nomeUtente, function(query1) {
								  if (query1 ==0){
									  $.getJSON("../php/reg_utente.php?request=insert&user="+nomeUtente+"&nome="+nome+"&cognome="+cognome+"&email="+email+"&pwd="+pwd, function(query4) {
										if(query4 == 1){
											window.location.replace("../html/login.html");
							  }
					  });
				  }
			  });
		  }
	  });

	}
}
