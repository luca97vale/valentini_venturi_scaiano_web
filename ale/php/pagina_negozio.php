<?php
session_start();
header('Content-Type: application/json');
if(isset($_GET["request"]))
{
  $servername = "localhost";
  $username = "root";
  $password = "";
  $dbname = "my_entrega";
  $conn = new mysqli($servername, $username, $password, $dbname);
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }
  switch ($_GET["request"]) {
    case 'session':
    $stmt = $conn->prepare("
    SELECT user FROM `utente` where user='".$_SESSION["user"]."'
    ");

    $stmt->execute();
	$result = $stmt->get_result();

    $output = array();
    while($row = $result->fetch_assoc()) {
      $output[] =	 $row;
    }
    $stmt->close();
    if(count($output) == 0){
      $arrayName = array('0');
      print json_encode($arrayName);
    }else{
      $arrayName = array('1');
      print json_encode($arrayName);
    }
    break;
    case 'forn':
      $negozio = $_GET["negozio"];
      $stmt = $conn->prepare("
      SELECT prod_specifico.id,prodotto.nome,prod_specifico.descrizione,prod_specifico.prezzo FROM `prod_specifico`,prodotto
      where prodotto.id=prod_specifico.id_prodotto and prod_specifico.user='".$negozio."'
      ");

      //$stmt->bind_param("ii", $start_row, $rows_per_page);
      $stmt->execute();

      $result = $stmt->get_result();


      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] =	 $row;
      }
      $stmt->close();
      print json_encode($output);
      break;

	  case 'send':
	   $idprod = $_GET["id"];
	   $qt= $_GET["qt"];
	   $user = $_SESSION["user"];

	   $stmt = $conn->prepare("SELECT `user` FROM `prod_specifico` WHERE `id` = ?");
	   $stmt->bind_param("s", $idprod);
	   $stmt->execute();
	   $result = $stmt->get_result();
		$output = array();
		while($row = $result->fetch_assoc()) {
			$output[] =	 $row;
		}
		$negozio = $output[0]['user'];
		//controllo se esiste un ordine con stato 0 e con negozio e user
		$stmt = $conn->prepare("SELECT * FROM `ordine` WHERE `user` = ? AND `user_fornitore` = ? AND `stato` = 0");
		$stmt->bind_param("ss", $user, $negozio);
		$stmt->execute();
		$result = $stmt->get_result();
		$num_of_rows = $result->num_rows;
		if($num_of_rows <= 0){
			$stmt = $conn->prepare("INSERT INTO `ordine`(`user`, `user_fornitore`, `notificato`) VALUES (?,?,0)");
			$stmt->bind_param("ss", $user, $negozio);
			$stmt->execute();
			$stmt = $conn->prepare("SELECT `id` FROM `ordine` WHERE `user` = ? AND `user_fornitore` = ? AND `stato` = 0");
			$stmt->bind_param("ss", $user, $negozio);
			$stmt->execute();
			$result = $stmt->get_result();
			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] =	 $row;
			}
			$idordine = $output[0]['id'];
			$stmt = $conn->prepare("INSERT INTO `dettagli_ordine`(`id_ordine`, `id_prodotto`, `quantita`) VALUES (?,?,?)");
			$stmt->bind_param("sss", $idordine, $idprod, $qt);
			$stmt->execute();
			$stmt->close();
		}else {
			$output = array();
			while($row = $result->fetch_assoc()) {
				$output[] =	 $row;
			}
			$idordine = $output[0]['id'];
			$stmt = $conn->prepare("SELECT `quantita` FROM `dettagli_ordine` WHERE `id_ordine` = ? AND`id_prodotto` = ?");
			$stmt->bind_param("ss", $idordine, $idprod);
			$stmt->execute();
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			if($num_of_rows > 0){
				$output = array();
				while($row = $result->fetch_assoc()) {
					$output[] =	 $row;
				}
				$qtprev = $output[0]['quantita'];
				$qtfin = $qtprev + $qt;
				$stmt = $conn->prepare("UPDATE `dettagli_ordine` SET `quantita`= ? WHERE `id_ordine` = ? AND `id_prodotto` = ?");
				$stmt->bind_param("sss", $qtfin, $idordine, $idprod);
				$stmt->execute();
				$stmt->close();
			} else {
				$stmt = $conn->prepare("INSERT INTO `dettagli_ordine`(`id_ordine`, `id_prodotto`, `quantita`) VALUES (?,?,?)");
				$stmt->bind_param("sss", $idordine, $idprod, $qt);
				$stmt->execute();
				$stmt->close();
			}
		}
		print json_encode(null);
		break;
	  //VISUALIZZARE E AGGIUNGERE RECENSIONI
		case 'get':
		$negozio = $_GET["param"];
		$stmt = $conn->prepare("SELECT * FROM `recensione` WHERE `userFornitore` = ?");
		$stmt->bind_param("s", $negozio);
		$stmt->execute();
		$result = $stmt->get_result();
		$output = array();
		while($row = $result->fetch_assoc()) {
			$output[] =	 $row;
		}
		$stmt->close();
		print json_encode($output);
		break;

		case 'send_rec':
		$negozio = $_GET["id"];
		$punteggio = $_GET["stelle"];
		$recensione = $_GET["recens"];
		$descrizione = "";
		$user = $_SESSION["user"];
		$stmt = $conn->prepare("INSERT INTO `recensione`(`userFornitore`, `userUtente`, `Stelle`, `descrizione`, `recenz`) VALUES (?, ?, ?, ?, ?)");
		$stmt->bind_param("sssss", $negozio, $user, $punteggio, $descrizione, $recensione);
		$stmt->execute();

		$stmt->close();
		print json_encode("");
		break;

    case 'info':
		$stmt = $conn->prepare("SELECT * FROM fornitore f, account a WHERE a.user = f.user AND f.user = ?");
		$stmt->bind_param("s", $_GET["param"]);
		$stmt->execute();
		$result = $stmt->get_result();
		$output = $result->fetch_assoc();
		$stmt->close();
		print json_encode($output);
		break;

    case 'fascia':
		$stmt = $conn->prepare("SELECT * FROM fornitore f, stabilisce s WHERE f.user = s.user AND f.user = ?");
		$stmt->bind_param("s", $_GET["param"]);
		$stmt->execute();
		$result = $stmt->get_result();
		$output = array();
		while($row = $result->fetch_assoc()) {
			$output[] =	 $row;
		}
		$stmt->close();
		print json_encode($output);
		break;
	}
}
?>
