<?php
session_start();
header('Content-Type: application/json');
if(isset($_GET["request"])){
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "my_entrega";
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	switch($_GET["request"]){
		case "current":
		$stmt = $conn->prepare("SELECT * FROM `account` WHERE `user` = ?");
		$stmt->bind_param("s", $user);
		$stmt->execute();
		$result = $stmt->get_result();
		$output = array();
		while($row = $result->fetch_assoc()) {
			$output[] =	 $row;
		}
		$stmt->close();
		print json_encode($output);
		break;
		case "update":
		$mex = "";
		$stmt = $conn->prepare("SELECT * FROM `account` WHERE `user` = ?");
		$stmt->bind_param("s", $user);
		$stmt->execute();
		$result = $stmt->get_result();
		$output = array();
		while($row = $result->fetch_assoc()) {
			$output[] =	 $row;
		}
		$stmt->close();
		if($_GET["nome"] != "" && $_GET["nome"] != $output[0]['nome']){
			$mex .= "<p>Il nome e' stato cambiato</p>";
			$stmt = $conn->prepare("UPDATE `account` SET `nome`= ? WHERE `user` = ?");
			$stmt->bind_param("ss",$_GET["nome"], $user);
			$stmt->execute();
			$stmt->close();
		}else{
			$mex .= "<p>Il nome non e' stato cambiato</p>";
		}
		if($_GET["cognome"] != "" && $_GET["cognome"] != $output[0]['cognome']){
			$mex .= "<p>Il cognome e' stato cambiato</p>";
			$stmt = $conn->prepare("UPDATE `account` SET `cognome`= ? WHERE `user` = ?");
			$stmt->bind_param("ss",$_GET["cognome"], $user);
			$stmt->execute();
			$stmt->close();
		}else{
			$mex .= "<p>Il cognome non e' stato cambiato</p>";
		}
		if($_GET["newpwd"] != "" && $_GET["newpwd"] != $output[0]['pwd']){
			$pwd = $_GET["newpwd"];
			if ($pwd == $_GET["verpwd"]){
				$mex .= "<p>E' stata cambiata la password</p>";
				$stmt = $conn->prepare("UPDATE `account` SET `pwd`= ? WHERE `user` = ?");
				$stmt->bind_param("ss", $pwd, $user);
				$stmt->execute();
				$stmt->close();
			}else {
				$mex .= "<p>Le due password inserite non sono uguali, pertanto non è stata aggiornata</p> ";
			}
		}else {
			$pwd = $output[0]['pwd'];
			$mex .= "<p>La password non è stata cambiata</p>";
		}
		$stmt = $conn->prepare("SELECT `e-mail` FROM `account` WHERE `e-mail` = ? and user != ?");
		$stmt->bind_param("ss", $_GET["newemail"],$user);
		$stmt->execute();
		$result = $stmt->get_result();
		$out = array();
		while($row = $result->fetch_assoc()) {
			$out[] = $row;
		}
		$stmt->close();
		if($out[0]['e-mail'] != $_GET["newemail"]){
			if($_GET["newemail"] != '' && $_GET["newemail"] != $output[0]['e-mail'] && filter_var($_GET["newemail"], FILTER_VALIDATE_EMAIL)){
				$email = $_GET["newemail"];
				$mex .= "<p>E' stata cambiata la E-Mail come richiesto </p>";
				$stmt = $conn->prepare("UPDATE `account` SET `e-mail`= ? WHERE `user` = ?");
				$stmt->bind_param("ss", $email, $user);
				$stmt->execute();
				$stmt->close();
			}else {
				if(!filter_var($_GET["newemail"], FILTER_VALIDATE_EMAIL)){
					$mex .= "<p>Inserisci una mail valida</p>";
				}
				$email = $output[0]['e-mail'];
				$mex .= "<p>La e-mail non è stata cambiata</p>";
			}
		}else{
			$mex .= "<p>Esiste un account con questa e-mail </p>";
		}
		/*$stmt = $conn->prepare("UPDATE `account` SET `e-mail`= ?,`pwd`= ? WHERE `user` = ?");
		$stmt->bind_param("sss", $email, $pwd, $user);
		$stmt->execute();
		$stmt->close();*/
		print json_encode($mex);
		break;

		case "editImage":
			$stmt = $conn->prepare("UPDATE `account` SET `image` = ? WHERE `user` = ?");
			$stmt->bind_param("bs", $_POST["img"], $user);
			$stmt->send_long_data(0, $_POST["img"]);

			$stmt->execute();
			$stmt->close();

			print json_encode("0");
			break;
	}
}
?>
