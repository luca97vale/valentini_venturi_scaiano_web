<?php
header('Content-Type: application/json');

if(isset($_GET["request"])) {
	$servername = "localhost";
	$username = "root";
	$password = "";
	$dbname = "my_entrega";

		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		switch ($_GET["request"]) {
		case "checkuser":
			$user = $_GET["user"];
			$stmt = $conn->prepare("SELECT * FROM `account` WHERE `user` = ?");
			$stmt->bind_param("s", $user);
			$stmt->execute();
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			if($num_of_rows > 0){
				$stmt->close();
				print json_encode(1);
			} else {
				$stmt->close();
				print json_encode(0);
			}
			break;
		case "checkemail":
			$email = $_GET["email"];
			$stmt = $conn->prepare("SELECT * FROM `account` WHERE `e-mail` = ?");
			$stmt->bind_param("s", $email);
			$stmt->execute();
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			if($num_of_rows > 0){
				$stmt->close();
				print json_encode(1);
			} else {
				$stmt->close();
				print json_encode(0);
			}
			break;
		case "checkpiva":
			$piva = $_GET["piva"];
			$stmt = $conn->prepare("SELECT * FROM `account` WHERE `partita_iva` = ?");
			$stmt->bind_param("s", $piva);
			$stmt->execute();
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			if($num_of_rows > 0){
				$stmt->close();
				print json_encode(1);
			} else {
				$stmt->close();
				print json_encode(0);
			}
			break;
		case "checkneg":
			$negozio = $_GET["negozio"];
			$stmt = $conn->prepare("SELECT * FROM `fornitore` WHERE `nome_negozio` = ?");
			$stmt->bind_param("s", $negozio);
			$stmt->execute();
			$result = $stmt->get_result();
			$num_of_rows = $result->num_rows;
			if($num_of_rows > 0){
				$stmt->close();
				print json_encode(1);
			} else {
				$stmt->close();
				print json_encode(0);
			}
			break;
		case "insert":
			$img = "data:application/octet-stream;base64,".base64_encode(file_get_contents($_FILES['img']['tmp_name']));
			$nome = $_GET["nome"];
			$cognome = $_GET["cognome"];
			$email = $_GET["email"];
			$user = $_GET["user"];
			$pwd = $_GET["pwd"];
			$negozio = $_GET["negozio"];
			$partitaiva = $_GET["piva"];
			$stmt = $conn->prepare("INSERT INTO `account`(`nome`, `cognome`, `e-mail`, `user`, `pwd`, `partita_iva`, `image`) VALUES (?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param("ssssssb", $nome, $cognome, $email, $user, $pwd, $partitaiva, $img);
			$stmt->send_long_data(6, $img);
			$stmt->execute();
			$stmt = $conn->prepare("INSERT INTO `fornitore`(`user`, `accettato`, `nome_negozio`) VALUES (?, '0', ?)");
			$stmt->bind_param("ss", $user, $negozio);
			$stmt->execute();
			$stmt->close();
			print json_encode(1);
			}}
?>
