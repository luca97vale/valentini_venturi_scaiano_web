$.getJSON("../php/session.php?request=admin", function(data) {
  if(data == 0){
    window.location.href = "../../ale/html/login.html?&err=-1";
  }
});

$(document).ready(function() {

  $.getJSON("../php/load_image.php", function(data) {
    if (data["image"] == null) {
      var image = "../base.jpg";
    } else {
      var image = data["image"];
    }
    $("#imgProfilo").attr("src", image);
  });

  $("input[value='Accetta fornitori']").click(function() {
    window.location.href = "../html/accept_Vendors.html", "_self";
  });

  $("input[value='Rimuovi utenti']").click(function() {
    window.location.href = "../html/remove_Users.html", "_self";
  });
});
