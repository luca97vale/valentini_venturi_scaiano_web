$.getJSON("../php/session.php?request=admin", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});

function searchbar(){
  if($("#search").val().length > 0){
    $("section#foods li").each(function(index) {
      if(!$(this).find("label").attr("class").toLowerCase().startsWith($("#search").val().toLowerCase())){
        $(this).css("display","none");
      } else {
        $(this).css("display","block");
      }
    });
  }else{
    $("section#foods li").each(function(index) {
      $(this).css("display","block");
    });
  }
}

$(document).ready(function() {

  $("#info").addClass("active");
  $('header').on("keyup", "#search", searchbar);

  $("#info").click(function() {
    $("#foods").css("display", "none");
    $("#times").css("display", "none");
    $("#reviews").css("display", "none");
    $("#details").css("display", "block");
    $("#info").addClass("active");
    $("#menu").removeClass("active");
    $("#time").removeClass("active");
    $("#recensioni").removeClass("active");
  });

  $("#menu").click(function() {
    $("#details").css("display", "none");
    $("#foods").css("display", "block");
    $("#times").css("display", "none");
    $("#reviews").css("display", "none");
    $("#info").removeClass("active");
    $("#time").removeClass("active");
    $("#recensioni").removeClass("active");
    $("#menu").addClass("active");
  });

  $("#time").click(function() {
    $("#foods").css("display", "none");
    $("#details").css("display", "none");
    $("#times").css("display", "block");
    $("#reviews").css("display", "none");
    $("#time").addClass("active");
    $("#menu").removeClass("active");
    $("#info").removeClass("active");
    $("#recensioni").removeClass("active");
  });

  $("#recensioni").click(function() {
    $("#foods").css("display", "none");
    $("#details").css("display", "none");
    $("#times").css("display", "none");
    $("#reviews").css("display", "block");
    $("#time").removeClass("active");
    $("#menu").removeClass("active");
    $("#info").removeClass("active");
    $("#recensioni").addClass("active");
  });

  $.getJSON("../php/vendor_details.php?request=getDetails", function(data) {
    if (data[0]["image"] == null) {
      var image = "../base.jpg";
    } else {
      var image = data[0]["image"];
    }
    $("#details img").attr("src", image);
    $("h1").text(data[0]["nome_negozio"]);
    $("input#name").val(data[0]["nome"]);
    $("input#surname").val(data[0]["cognome"]);
    $("input#email").val(data[0]["e-mail"]);
    $("input#p_iva").val(data[0]["partita_iva"]);
  });

  $.getJSON("../php/vendor_details.php?request=getMenu", function(data) {
    var html_code = "<ul>";
    for(var i = 0; i < data.length; i++){
      html_code += '<li>';
      html_code += '<label for="'+data[i]["id"]+'" class="'+data[i]["nome"]+'">'+data[i]["nome"]+'</label>';
      html_code += '<input type="text" id="'+data[i]["id"]+'" name="prezzo" value="'+data[i]["Prezzo"]+' €" disabled>';
      html_code += '<label >'+data[i]["Descrizione"]+'</label>';
      html_code += '</li>';
    }
    html_code += '</ul>';

    $("#foods").append(html_code);
  });

  $.getJSON("../php/vendor_details.php?request=getTime", function(data) {
    if (data.length != 0) {
      var html_code = "";
      html_code += '<table>'+
      '<thead>'+
        '<tr>'+
          '<th>Ora Inizio</th>'+
          '<th>Ora Fine</th>'+
          '<th>Numero Clienti</th>'+
        '</tr>'+
      '</thead>'+
      '<tbody id="contenuto">'+
      '</tbody>'+
      '</table>';

      $("#times").append(html_code);

      for(var i = 0; i < data.length; i++) {
        html_code = "";
        html_code += '<tr>'+
          '<td>'+data[i]["ora_inizio"].substring(0, 5)+'</td>'+
          '<td>'+data[i]["ora_fine"].substring(0, 5)+'</td>'+
          '<td>'+data[i]["n_clienti"]+'</td>'+
          '</tr>';
        $("tbody#contenuto").append(html_code);
      }
    }
  });

  $.getJSON("../php/vendor_details.php?request=getReviews", function(data) {
    html_code = "";
    for(var i = 0; i < data.length; i++){
      html_code += '<div class="containerReviews">';
      html_code += '<h2><span>'+data[i]["userUtente"]+'</span> </h2>';
      var stelle = data[i]["Stelle"];
      for(j=1;j<=5;j++){
        if(j<=stelle){
          html_code += '<span class="fa fa-star checked" ></span>';
        }else{
          html_code += '<span class="fa fa-star"></span>';
        }
      }
      html_code += '<p>'+data[i]["recenz"]+'</p>'
      html_code += '</div>';
    }
    $("#reviews").append(html_code);
  });
});
