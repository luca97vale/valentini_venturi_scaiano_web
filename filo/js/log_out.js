$(document).ready(function() {

  $(".fa-caret-up").hide();
  $("#logout").css("display", "none");

  $(".fa-caret-down").click(function() {
    $(this).hide();
    $("#logout").show();
    $(".fa-caret-up").show();
    $(".fa-caret-up").css("color", "grey");
  });

  $(".fa-caret-up").click(function() {
    $(this).hide();
    $("#logout").hide();
    $(".fa-caret-down").show();
  });

  $("#logout").click(function() {
    $.ajax({url: '/filo/php/destroy_session.php'});
    window.location.href = "../../ale/html/login.html";
  });
});
