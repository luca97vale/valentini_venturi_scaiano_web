<?php
header('Content-Type: application/json');
include "connection.php";

$conn = setConnection();

$stmt = $conn->prepare("DELETE FROM account WHERE user = ?");
$stmt->bind_param("s", $_POST["user"]);
$stmt->execute();

if ($stmt->affected_rows > 0) {
  $response_array['status'] = 'success';
} else {
  $response_array['status'] = 'error';
}

$stmt->close();
print json_encode($response_array);
?>
