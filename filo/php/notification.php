<?php
session_start();
header('Content-Type: application/json');
include "connection.php";

if(isset($_GET["request"]))
{
  $conn = setConnection();

  switch ($_GET["request"]) {
    case "loadNotifications":
      $user = $_SESSION["user"];
      $stmt = $conn->prepare("SELECT o.*, f.nome_negozio FROM ordine o, fornitore f WHERE o.user_fornitore = f.user AND o.user = ? AND o.notificato < o.stato AND o.stato > 1");
      $stmt->bind_param("s", $user);
      $stmt->execute();
      $result = $stmt->get_result();

      $output = array();
      while($row = $result->fetch_assoc()) {
        $output[] = $row;
      }

      $stmt->close();
      print json_encode($output);

      break;

    case "updateNotifications":
      $stmt = $conn->prepare("UPDATE ordine SET notificato = ? WHERE id = ?");
      $stmt->bind_param("ii", $_POST["stato"], $_POST["id"]);
      $stmt->execute();

      $output = $stmt->affected_rows;

      $stmt->close();
      print json_encode($output);

      break;
  }
}
?>
