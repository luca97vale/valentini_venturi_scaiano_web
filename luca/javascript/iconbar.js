$(document).ready(function() {

  $("span.fa-search").click(function() {
    $("nav#icons").css("display", "none");
    $("nav#searchbar").css("display", "inline-block");
  });

  $("span.fa-times").click(function() {
    $(this).prev().val("");
    $(this).css("display", "none");
    searchbar();
  });

  $("span.fa-arrow-left").click(function() {
    $("nav#searchbar").css("display", "none");
    $("nav#icons").css("display", "block");
  });

  $("input#search").on("input", function() {
    if( $(this).val().length !== 0 ) {
      $("span.fa-times").css("display", "inline-block");
    }else{
      $("span.fa-times").css("display", "none");
    }
  });
});
