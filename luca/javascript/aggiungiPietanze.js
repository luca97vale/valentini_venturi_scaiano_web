var side_open=0;
$.getJSON("../php/pietanzeFornitore.php?request=session", function(data) {
  if(data == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});
$(document).ready(function(){
  ready();
});
function ready(){
  $.getJSON("../php/aggiungiPietanze.php?request=add", function(data) {
    var html_code = "";
    html_code += '<div id="addP">';
    html_code += '<h1>Inserisci un prodotto</h1>';
    html_code += '<label for="addProdotto" hidden>lb</label><input type="text" name="nomeProdotto" id="addProdotto" placeholder="Nome prodotto">';
    html_code += selectCategory();
    html_code += '<button type="button" class="action-button shadow animate orange" onclick="clkAdd()" name="aggiungi">Aggiungi</button> </li></ul>';
    html_code += '</div>';
    html_code += '<ul id="pietanzeUl">';
    for(var i = 0; i < data.length; i++){
      html_code += '<li><div><label >'+data[i]["nome"]+'</label>';
      html_code += '<button type="button" class="action-button shadow animate orange" id="'+data[i]["id"]+'"name="aggiungi">Aggiungi</button>';
      html_code += '<label for="price'+data[i]["id"]+'" hidden>lb</label><input type="number" name="prezzo" id="price'+data[i]["id"]+'" min="1.00" max="100.00" step="0.10" value="2.0"></li></div> ';
    }
    html_code += '</ul>';
    $("#pietanze").html(html_code);
    for(var i = 0; i < data.length; i++){
      var desc = $("#desc"+data[i]["id"]);
      $("#"+data[i]["id"]+"").click({id: data[i]["id"]}, clickbtn);
      function clickbtn(event){
        var descrizione = prompt("Inserisci gli ingredienti del prodotto:","");
        if (descrizione == null || descrizione == "") {
        }else{
          clickAggiungi(event.data.id,document.getElementById("price".concat(event.data.id)).value,descrizione);
        }
      }
    }
  });
  $.getJSON("../php/pietanzeFornitore.php?request=categ", function(data) {
    var html_code = "<ul>";
    for(var i = 0; i < data.length; i++){
      html_code += '<li><label class="container">'+data[i]["categoria"]+'';
      html_code += '<input type="checkbox" id="'+data[i]["categoria"]+'" onclick="clickCateg('+data[i]["categoria"]+')" value="'+data[i]["categoria"]+'">';
      html_code += '<span class="checkmark"></span>';
      html_code += '</li></label>';
    }
    html_code+="</ul>";
    $("#mySidenav").append(html_code);
  });
  window.addEventListener('click', function(e){
    if(side_open){
      if (document.getElementById('mySidenav').contains(e.target)){
      } else{
        if(document.getElementById('side').contains((e.target))){}
        else{
          closeNav();
        }
      }
    }
  });
}
function clickAggiungi(id,prezzo,descrizione){
  $.getJSON("../php/pietanzeFornitore.php?request=user", function(data) {
    session = data[0];
    $.getJSON("../php/aggiungiPietanze.php?request=query&session="+session+"&prezzo="+prezzo+"&descrizione="+descrizione+"&id="+id+"", function(data) {
    });
  });
  $("#pietanze").html(' ');
  ready();
}
function getPrezzo(id){
  return +$("#price"+id).val();
}
function selectCategory(){
  var html_code ='';
  $.ajaxSetup({
    async: false
  });
  $.getJSON("../php/aggiungiPietanze.php?request=categ", function(data) {
    html_code += '<div class="select">';
    html_code += '<label for="categoria" hidden>lb</label><select id="categoria" name="categoria">';
    for(var i = 0; i < data.length; i++){
      html_code += '<option value="'+data[i]["categoria"]+'">'+data[i]["categoria"]+'</option>';
    }
    html_code += '</select>';
    html_code += "</div>";
  });
  $.ajaxSetup({
    async: true
  });
  return html_code;
}
function clkAdd(){
  var prodotto = $("#addProdotto").val();
  var categoria = $("#categoria").val();
  if(prodotto!=''){
    $.getJSON("../php/aggiungiPietanze.php?request=addDB&prodotto="+prodotto+"&categoria="+categoria, function(data) {
    });
    $.getJSON("../php/aggiungiPietanze.php?request=getID&nome="+prodotto+"&categoria="+categoria, function(data) {
      for(var i = 0; i < data.length; i++){
        loadPage(data[i]["id"],prodotto);
      }
    });
  }
}
function loadPage(id,nome){
  var html_code = '';
  html_code += '<div><li><label >'+nome+'</label>';
  html_code += '<button type="button" class="action-button shadow animate orange" id="'+id+'"name="aggiungi">Aggiungi</button>';
  html_code += '<label for="price'+data[i]["id"]+'" hidden>lb</label><input type="number" name="prezzo" id="price'+id+'" min="1.00" max="100.00" step="0.10" value="2.0"></li></div> ';
  $("ul#pietanzeUl").append(html_code);
  //riprendere dalla chiusura del tag ul, eliminarlo e aggiungere questo li
}
function openNav() {
  $("#icons").css("display","none");
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
  side_open = 1;
}

function closeNav() {
  $("#icons").css("display","block");
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
  side_open = 0;
}

function clickCateg(categoria){
  $.getJSON("../php/pietanzeFornitore.php?request=categ",function(data) {
    var arr = new Array();
    for(var i = 0; i < data.length; i++){
      if($("#"+data[i]["categoria"]).prop("checked")){
        arr.push(data[i]["categoria"]);
      }
    }
    $.getJSON("../php/aggiungiPietanze.php?request=forn",{arr:arr} ,function(data) {
      var html_code = "";
      html_code += '<div id="addP">';
      html_code += '<p>Inserisci un prodotto</p>';
      html_code += '<label for="addProdotto" hidden>lb</label><input type="text" name="nomeProdotto" id="addProdotto" placeholder="Nome prodotto">';
      html_code += selectCategory();
      html_code += '<button type="button" class="action-button shadow animate orange" onclick="clkAdd()" name="aggiungi">Aggiungi</button> </li></ul>';
      html_code += '</div>';
      html_code += '<ul id="pietanzUl">';
      for(var i = 0; i < data.length; i++){
        html_code += '<li><div><label >'+data[i]["nome"]+'</label>';
        html_code += '<button type="button" class="action-button shadow animate orange" id="'+data[i]["id"]+'"name="aggiungi">Aggiungi</button>';
        html_code += '<label for="price'+id+'" hidden>lb</label><input type="number" name="prezzo" id="price'+data[i]["id"]+'" min="1.00" max="100.00" step="0.10" value="2.0"></li></div> ';
      }
      html_code += '</ul>';
      $("#pietanze").html(html_code);
    });
  });
}
