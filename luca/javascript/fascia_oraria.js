$.getJSON("../php/fascia_oraria.php?request=session", function(data) {
  if(data[0] == 0){
    window.location.replace("../../ale/html/login.html?&err=-1");
  }
});
$(document).ready(function() {
  ready();
});
function save(){
  $.getJSON("../php/fascia_oraria.php?request=get", function(data) {
    for(var i = 0; i < data.length; i++){
      var inizio = data[i]["ora_inizio"].substring(0,2);
      var fine = data[i]["ora_inizio"].substring(0,2);
      var num = $("input[type='number'][id='"+inizio+"']").val();
      if($("."+data[i]["ora_inizio"].substring(0,2)).prop("checked")){
        //verifico se il range e' gia' presente perche' altrimenti non si tratterebbe di un insert ma di un update
        $.getJSON("../php/fascia_oraria.php?request=add&num="+num+"&inizio="+inizio+"&fine="+fine, function(data) {
        });
        $.getJSON("../php/fascia_oraria.php?request=add_&num="+num+"&inizio="+inizio+"&fine="+fine, function(data) {
        });
      }else{
        $.getJSON("../php/fascia_oraria.php?request=delete&inizio="+inizio+"&fine="+fine, function(data) {
        });
      }
    }
  });
}
function ready(){
  $.getJSON("../php/fascia_oraria.php?request=get", function(data) {
    var html_code = "";
    html_code += "<table>";
    for(var i = 0; i < data.length; i++){
      var cls = data[i]["ora_inizio"].substring(0,2);
      if(i==0){
        html_code += "<tr>";
        html_code += "<th id='vuoto'></th>";
        html_code += "<th id='OraInizio'>Ora Inizio</th>";
        html_code += "<th id='OraFine'>Ora Fine</th>";
        html_code += "<th id='third'>N.Persone</th>";
        html_code += "</tr>";
      }
      html_code += "<tr>";
      html_code += "<th id='f"+i+"' class='container'><label for='zz"+i+"' hidden>lb</label>";
      html_code += '<input id="zz'+i+'" type="checkbox" class="'+cls+'">';
      html_code += '<span id="s'+cls+'" class="checkmark"></span></th>';
      var ora=data[i]["ora_inizio"];
      ora = ora.substring(0,5);
      html_code += '<th id="oraI'+i+'">'+ora+'</th>'
      ora=data[i]["ora_fine"];
      ora = ora.substring(0,5);
      html_code += '<th id="oraF'+i+'">'+ora+'</th>'
      html_code += '<th id="persone'+i+'"><label for="'+data[i]["ora_inizio"].substring(0,2)+'" hidden>lb</label><input type="number" value="0" id="'+data[i]["ora_inizio"].substring(0,2)+'" name="quantity" min="1" max="50"></th>';
      html_code += '</tr>';
  }
  html_code += "</table>";
  html_code += '<button type="button" class="action-button shadow animate orange" onclick="save()" >Save</button>';
  $("#orari").html(html_code);
  var inizio;
  $.getJSON("../php/fascia_oraria.php?request=update", function(data) {
    for(var i = 0; i < data.length; i++){
      $("."+(data[i]["ora_inizio"].substring(0,2))).attr("checked",true);
      inizio = data[i]["ora_inizio"].substring(0,2);
      showPerson(inizio);
    }
  });
});
}
function showPerson(inizio){
  $.getJSON("../php/fascia_oraria.php?request=getpersone&inizio="+inizio+"", function(dt) {
    $("input[type='number'][id='"+inizio+"']").val(dt[0]["n_clienti"]);
  });
}
